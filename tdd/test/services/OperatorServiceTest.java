package services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OperatorServiceTest {

    private OperatorService op;

    @BeforeEach
    public void init() {
        op = new OperatorService();
    }

    @Test
    public void testDivision() {
        assertNotNull(op.substract(5, 3));
        assertEquals(op.substract(5,3), 2);
        assertEquals(op.substract(2,3), -1);
    }

    @Test
    public void testDivide() {
        assertEquals(op.divide(5,1), 5);

        try {
            op.divide(5,0);
            fail("Should throw exception");
        } catch (Exception e) {
            assertTrue(true);
            assertTrue(e instanceof CustomException);
        }

        assertThrows(CustomException.class, () -> {
            op.divide(5,0);
        });

    }


}
