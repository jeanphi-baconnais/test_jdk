package services;

public class CustomException extends ArithmeticException {

    public CustomException(String m){
        super(m);
    }
}
