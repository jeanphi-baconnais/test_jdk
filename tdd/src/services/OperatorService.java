package services;

public class OperatorService {

    public int substract(int i, int i1) {
        return i - i1;
    }

    public int divide(int i, int i1) {
        if (i1 == 0) {
            throw new CustomException("Division 0, pas bien");
        }

        return 5 / i1;
    }
}
